const fs = require('fs');
const { exit } = require('process');
const readline = require("readline");

let options = ["-n", "-i", "-e", "-f"] // Opciones disponibles

const arr = process.argv.slice(2) //Cortar los dos primeros parámetros de la línea de comando

if((arr.length-1)<2){ //Condicional para cuando los parámetros sean menor a 2, muestre un mensaje indicando los requisitos mínimos.
    console.log("Los parámetros indicados no satisfacen los requisitos. (opción - comando - archivo)");
    exit()
}else{ 

    if(fs.existsSync('./'+arr[arr.length-1])){// Condicional para verificar que el archivo ingresado exista.

        console.log('\n'+"El archivo ["+ arr[arr.length-1] +"] existe")

        if (findOptions(options, arr[0])){ // Condicional para verificar que la opción ingresada exista.
            console.log("El comando ["+ arr[0] +"] existe"+'\n')
            let command1=arr[1]
            
            if(arr[0]=='-i'){
                command1=arr[2]
            }

            if((arr[0]=='-n' || arr[0]=='-e') && (command1[0]!='s' || command1[1]!='/')){ //Conidicional para verificar que el comando se sustitución sea válido.
                console.log("El comando de sustitución ["+ command1 +"]  no es válido. (s/buscar/reemplazar/bandera(oopcional))") // Mensaje para indicar cómo debería ser ingresado el comando de sustitución en caso de que se ingrese de manera errónea.
            }

            if((command1[0]=='s' && command1[1]=='/') || arr[0]=='-f'){ //Condicional para validar que el comando de sustitución sea válido.
                command1 =command1.substr(command1.indexOf('s/')+2) //Sustraemos el comando de sustitución para hacerlo más simple.
                command1 = command1.split('/')// Dividimos el comando de sustitución para que sea más simple.

                let lines = readline.createInterface({ //Añadimos las líneas del archivo ingresado.
                    input: fs.createReadStream('./'+arr[arr.length-1])
                });

                commands(lines,command1) //Invocamos la función para realizar las acciones.
            }
        }else{ // En caso de que el comando no exista,nos arroja un mensaje.
            console.log("La opción ["+ arr[0] +"] no existe.")
            exit()
        }
    }else{ // En caso de que el archivo no exista, nos arroja un mensaje.
        console.log("El archivo [" + arr[arr.length-1] +"] no existe.")
        exit()
    }
}

function findOptions(options, option){ //Función para validar que la opción ingresada exista.
  
    for (i=0; i<options.length; i++){ //Cíclo for para recorrer el arreglo.

        if(option==options[i]){ //Condicional para comparar la opción ingresada con las disponibles.
            return true //Si existe, retornamos un 'true'            
        }
    }
    return false // Si la opción no existe, retornamos un 'false'
}   

function commands(lines, command){ //Función donde realizamos las acciones de las opciones ( -n, -i, -e, -f).

    if(arr[0]=='-n'){ //Condicional para cuando la opción ingresada sea la '-n'
        var word1 = new RegExp(command[0]); //Declaramos una variable con una expresión regular.
        lines.on("line", linea => { //Recorremos la interfaz que contiene las líneas de el archivo de texto ingresado.
            if(command[command.length-1]=='p'){ //Condicional para verificar si la bandera es p y así imprimir la línea.
                if(word1.test(linea)){//Condicional para buscar simulitud en el patrón ingresado.
                    console.log(linea.replace(command[0],command[1]));
                }
            }
        });
    }

    if(arr[0]=='-i'){ //Condicional para cuando la opción ingresada sea la '-i'       

        var word1 = new RegExp(command[0]); //Expresión regular para falicitar la búsqueda del patrón
        var nameFile = arr[arr.length-1].split('.txt')// Tomamos el nombre del archivo para crear una copia

        fs.truncate(nameFile[0]+".txt."+arr[1], 0, function() { //Vaciamos el archivo copia si ya existe
        });

        lines.on("line", linea => { //Recorremos las líneas del primer archivo y las agregamos a la copia
            fs.appendFile(nameFile[0]+".txt."+arr[1],linea+'\n',err =>{
                if(err){
                    console.log(err)
                }
            })
        }); 

        console.log("Copia del archivo creada.")
        
        fs.truncate(arr[arr.length-1], 0, function() { //Vaciamos el archivo original
        });
      
        lines.on("line", linea => { // Recorremos las líneas para realizar los cambios
            if(command[command.length-1]=='p'){ // Condicional para encontrar la bandera y realizar el cambio
                if(word1.test(linea)){ //Busamos la coincidencia en la línea 
                    fs.appendFile(arr[arr.length-1],linea.replace(command[0],command[1])+'\n',(err) =>{ // Agregamos las líneas con los cambios
                        if(err){
                            console.log(err)
                        }
                    })
                }
            }                
       });    

       console.log("Archivo modificado.")
    }

    if(arr[0]=='-e'){ //Condicional para cuando la opción ingresada sea la '-e'
        
        let index = [] //Array para guardar los índices de cada comando de sustitución
        let comandos = [] //Array para guardar los comandos
        let count = 0 // Variable contador

        for (i=0; i<arr.length; i++){ // Cíclo para recorrer los parámetros
            if(arr[i]=='-e'){ // Condicional para validar si el elemento del array sea igual a '-e'
                index[count]=i+1 // Guardamos el índice+1 en el array de índices
                if(/s\//.test(arr[index[count]])){ // Condicional para encontrar la coincidencia
                    comandos[count]=arr[index[count]].substr(arr[index[count]].indexOf('s/')+2) //Tomamos la parte despues del 's/'
                    comandos[count]=comandos[count].split('/') // Dividimos el comando de sustitución
                }
                else{
                    console.log("El comando de sustitución ["+arr[index[count]]+"] no es válido. (s/buscar/reemplazar/bandera(opcional))")
                    exit()
                }
                count=count+1 //Aumentamos el contador, para seguir guardando índices a medida que avance el cíclo           
            }
        }      

            lines.on("line", linea => { //Recorremos las líneas
                for(i=0; i<index.length; i++){ //Recorremos el array de índices
                    var word2 = new RegExp(comandos[i][0]); //Guardamos la palabra a buscar como expresión regular                    
                    if(word2.test(linea)){ // Condicional para buscar coincidencia en la línea
                        
                        if(comandos[i][comandos[i].length-1]=='g'){ // Condicional para encontrar la bandera g al final del comando de sustitución
                            word2=new RegExp(comandos[i][0],'g') // Cambiamos el comando de sustitución añadiendole la g
                            linea=linea.replace(word2,comandos[i][1]) //Realizamos el cambio en toda la línea
                            break; // Rompemos el cíclo debido a que ya se cambiaron todas las coincidencias de la línea
                        }
                        
                        linea=linea.replace(comandos[i][0],comandos[i][1]) // Reemplazamos
                        if(comandos[i][comandos[i].length-1]=='p'){ // Condicional para buscar encontrar la bandera p al final de l comando de sustitución
                            console.log(linea)
                        }                        
                    }              
                }                
                console.log(linea)
            });
    }

    if(arr[0]=='-f'){ // Condiconal para cuando la opción sea '-f'   

        if(fs.existsSync('./'+arr[arr.length-2])){ // Validamos que el archivo de los comandos exista

            var commands_file = fs.readFileSync('./'+arr[arr.length-2]).toString().split('\n') // Array donde se guardarán todos los comandos de ejecución del archivo
            var count=0

            for (i=0; i<commands_file.length; i++){ //Cíclo donde recorremos el array de los comandos del archivo

                if(/\r/.test(commands_file[i])){ //buscamos la coincidencia
                    commands_file[i]=commands_file[i].replace(/\r/,'') // eliminamos la coincidencia que se daba
                }
                    if(/s\//.test(commands_file[i])){ // Buscamos la coincidencia
                        commands_file[i]=commands_file[i].substr(commands_file[i].indexOf('s/')+2) // Tomamos la parte del comando que sigue después de 's/'
                        commands_file[i]=commands_file[i].split('/')// Dividimos el comando de ejecución                        
                    }
                    else{
                        console.log("El comando de sustitución ["+ commands_file[i] +"] no es válido. (s/buscar/reemplazar/bandera(opcional))")
                        exit()
                    }
                    count=count+1               
            }

            lines.on("line", linea => { //Recorremos las líneas del segundo archivo
                for(i=0; i<commands_file.length; i++){ // Cíclo para recorrer el array de los comandos del archivo  
                    var word3 = new RegExp(commands_file[i][0]); //Creamos una expresión regular con la coincidencia                 
                        if(word3.test(linea)){ //Buscamos la coincidencia en la línea
                            if(commands_file[i][commands_file[i].length-1]=='g'){ // Buscamos la bandera 'g' al final del comando de sustitución
                                word3=new RegExp(commands_file[i][0],'g') // Modificamos la expresión regular
                                linea=linea.replace(word3,commands_file[i][1]) // Reemplazamos todas las coincidencias encontradas en la línea
                                break; // Rompemos el cíclo ya que se reemplazaron todas las coincidencias en esa línea
                            }
                            linea=linea.replace(commands_file[i][0],commands_file[i][1]) // Reemplazamos
                            if(commands_file[i][commands_file[i].length-1]=='p'){ // Buscamos la bandera 'p' al final del comando de sustitución
                                console.log(linea)
                            }                        
                        }              
                }                
            console.log(linea)
        });

        } else{
            console.log("El archivo ["+arr[arr.length-2] +"] no existe.")
            exit()
        }

    }

}